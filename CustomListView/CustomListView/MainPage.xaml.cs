﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CustomListView
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        

        void Handle_NavigateToWebNavigationListView(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new WebNavigationListView());
        }
    }
}
