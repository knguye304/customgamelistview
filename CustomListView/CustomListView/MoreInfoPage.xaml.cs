﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CustomListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfoPage : ContentPage
	{
		public MoreInfoPage ()
		{
			InitializeComponent ();
		}

        public MoreInfoPage(WebsiteItem game)
        {
            InitializeComponent();
            BindingContext = game;
        }
       

	}
}