﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CustomListView
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WebNavigationListView : ContentPage
    {

        public WebNavigationListView()
        {
            InitializeComponent();
            PopulateListView();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            WebsiteItem itemTapped = (WebsiteItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }

        private void PopulateListView()
        {
            var listOfWebSites = new ObservableCollection<WebsiteItem>()
            {
               new WebsiteItem(){websiteName="Guild Wars 2", url = "https://www.guildwars2.com/en/",ImageText="This is a mmorpg developed by ArenaNet.", IconSource ="gw2.png", MoreInfo="GW2 is the best mmorpg." },
               new WebsiteItem(){websiteName="League of Legends", url = "https://na.leagueoflegends.com/en/",ImageText="This is a MOBA developed by Riot Games.", IconSource ="league.png",MoreInfo="League of legends sucks" },
               new WebsiteItem(){websiteName="Skyrim", url = "https://en.wikipedia.org/wiki/The_Elder_Scrolls_V:_Skyrim",ImageText="This is a RPG developed by Bethesda", IconSource ="skyrim.png", MoreInfo="Skyrim is a fun rpg" },
               new WebsiteItem(){websiteName="Age of Empires 3", url = "https://www.ageofempires.com/games/aoeiii/",ImageText="This is a RTS developed by Microsoft", IconSource ="aoe.png",MoreInfo="This game is really old"},
               new WebsiteItem(){websiteName="Fire Emblem Heroes", url = "https://fire-emblem-heroes.com/en/",ImageText="This is a mobile game developed by Intellent Systems", IconSource ="feh.png", MoreInfo="Fire emblem heroes is a gacha mobile game."},
               new WebsiteItem(){websiteName="Mario Kart 8", url = "https://www.mariowiki.com/Mario_Kart_8_Deluxe",ImageText="This is a racing game developed by Nintendo", IconSource ="mario.png",MoreInfo="This game is really fun with friends."},

            };

            WebsiteListView.ItemsSource = listOfWebSites;

        }

        void Handle_Clicked(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var game = (WebsiteItem)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreInfoPage(game));
        }

        void Handle_Delete(object sender, EventArgs e)
        {

        }
  
    }
}