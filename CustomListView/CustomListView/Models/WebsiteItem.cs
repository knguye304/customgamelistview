﻿using System;
using Xamarin.Forms;

namespace CustomListView
{
    public class WebsiteItem
    {
        public string websiteName
        {
            get;
            set;

        }

        public string url
        {
            get;
            set;

        }
        public string ImageText
        {
            get;
            set;
        }
        public ImageSource IconSource
        {
            get;
            set;

        }

        public string MoreInfo
        {
            get;
            set;
        }


    }
}